Cenário: Comprar manga
Dado que estou na lista de compras
Quando seleciono o produto Manga
E informo a quantidade 10
Quando informo o valor unitário 5 reais
E confirmo a compra
Então terei de pagar 50 reais

Cenário: Comprar jaca
Dado que estou na lista de compras
Quando seleciono o produto Jaca
E informo a quantidade 32
Quando informo o valor unitário 2 reais
E confirmo a compra
Então terei de pagar 64 reais