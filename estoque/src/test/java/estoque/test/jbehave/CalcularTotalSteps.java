package estoque.test.jbehave;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.WebDriver;

import estoque.test.selenium.CalcularTotalTest;

public class CalcularTotalSteps {

	private CalcularTotalTest calcularTotal;
	private static final String QUANTIDADE_ELEMENT = "quantidade";
	private static final String VALOR_UNITARIO_ELEMENT = "valorUnitario";
	private static final String PATH_WEB_PAGE = "file:///C:\\Users\\admdsilva\\Desktop\\estoque-2018-2\\estoque\\src\\main\\webapp\\lista-compras.html";

	public CalcularTotalSteps(WebDriver driver) {
		calcularTotal = new CalcularTotalTest(driver);
	}

	@Given("estou na lista de compras")
	public void abrirPaginaWeb() {
		calcularTotal.abrirPaginaWeb(PATH_WEB_PAGE);
	}

	@When("seleciono o produto $produto")
	public void selecionarProduto(String produto) {
		calcularTotal.selecionarProduto(produto);
	}

	@When("informo a quantidade $quantidade")
	public void informarQuantidade(Integer quantidade) {
		calcularTotal.informarValor(quantidade, QUANTIDADE_ELEMENT);
	}

	@When("informo o valor unit�rio $valorUnitario reais")
	public void informarValorUnitario(Integer valorUnitario) {
		calcularTotal.informarValor(valorUnitario, VALOR_UNITARIO_ELEMENT);
	}

	@When("confirmo a compra")
	public void confirmarCompra() {
		calcularTotal.clicarCalcularBtn();
	}

	@Then("terei de pagar $valorTotal reais")
	public void verificarValorTotal(Integer valorTotal) {
		calcularTotal.verificarValorTotal(valorTotal);
	}

}
