package estoque.test.browsers;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.openqa.selenium.chrome.ChromeDriver;

import estoque.test.config.JbehaveConfig;
import estoque.test.jbehave.CalcularTotalSteps;

public class ChromeDriverSelenium extends JbehaveConfig {

	private static final String DRIVER = "webdriver.chrome.driver";
	private static final String DRIVER_LOCATION_CHROME = "chromedriver.exe";

	@Override
	public InjectableStepsFactory stepsFactory() {
		System.setProperty(DRIVER, PATH_DRIVER + DRIVER_LOCATION_CHROME);
		ChromeDriver driver = new ChromeDriver();
		return new InstanceStepsFactory(configuration(), new CalcularTotalSteps(driver));
	}

}
