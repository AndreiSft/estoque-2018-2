package estoque.test.browsers;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import estoque.test.config.JbehaveConfig;
import estoque.test.jbehave.CalcularTotalSteps;

public class FirefoxDriverSelenium extends JbehaveConfig {

	private static final String DRIVER = "webdriver.gecko.driver";
	private static final String DRIVER_LOCATION_FIREFOX = "geckodriver.exe";

	@Override
	public InjectableStepsFactory stepsFactory() {
		System.setProperty(DRIVER, PATH_DRIVER + DRIVER_LOCATION_FIREFOX);
		FirefoxOptions firefoxOptions = new FirefoxOptions();
		firefoxOptions.setCapability("marionette", true);
		WebDriver driver = new FirefoxDriver(firefoxOptions);
		return new InstanceStepsFactory(configuration(), new CalcularTotalSteps(driver));
	}

}
