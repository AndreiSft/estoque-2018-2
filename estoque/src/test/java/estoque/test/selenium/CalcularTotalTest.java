package estoque.test.selenium;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CalcularTotalTest {

	private WebDriver driver;
	private WebElement element;

	private static final String XPATH_SELECT = "//td[2]/select[1]";
	private static final String XPATH_JACA = "//td[2]/select[1]/option[2]";
	private static final String XPATH_MANGA = "//td[2]/select[1]/option[3]";

	public CalcularTotalTest(WebDriver driver) {
		this.driver = driver;
	}

	public void abrirPaginaWeb(String page) {
		driver.get(page);
	}

	public void selecionarProduto(String produto) {
		element = driver.findElement(By.xpath(XPATH_SELECT));
		element.click();
		switch (produto) {
			case "Jaca" :
				element = driver.findElement(By.xpath(XPATH_JACA));
				element.click();
				break;
			case "Manga" :
				element = driver.findElement(By.xpath(XPATH_MANGA));
				element.click();
				break;
		}
	}

	public void informarValor(Integer valor, String idElement) {
		element = driver.findElement(By.id(idElement));
		element.sendKeys(valor.toString());
	}

	public void clicarCalcularBtn() {
		element = driver.findElement(By.id("calcularBtn"));
		element.click();
	}

	public void verificarValorTotal(Integer mediaEsperada) {
		element = driver.findElement(By.id("valorTotal"));
		Integer mediaAtual = Integer.parseInt(element.getAttribute("value"));
		Assert.assertEquals(mediaEsperada, mediaAtual);
	}

}
